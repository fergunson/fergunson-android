package com.example.fsouza.appempresas.api;

import com.example.fsouza.appempresas.models.EnterprisesList;
import com.example.fsouza.appempresas.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by fsouza on 16/07/17.
 * interface ApiService com os padrões de mensagens a serem enviadas para o servidor da API
 */

public interface ApiService {
    //post com um jSON de login e senha no body onde receberemos 3 tokens no header da resposta
    @POST("users/auth/sign_in")
    Call<User> validateUser(@Body User user);

    //GET com os 3 tokens de acesso e o Query com o nome da empresa que queremos buscar
    @GET("enterprises")
    Call<EnterprisesList> enterpriseList(
            @Header("access-token") String token,
            @Header("client") String client,
            @Header("uid") String uid,
            @Query("name") String enterpriseName);
}