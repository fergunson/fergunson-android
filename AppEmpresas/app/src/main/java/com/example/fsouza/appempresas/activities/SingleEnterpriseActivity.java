package com.example.fsouza.appempresas.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fsouza.appempresas.R;
import com.example.fsouza.appempresas.models.Enterprise;

/**
 * Classe SingleEnterpriseActivity onde irão ser mostrados os detalhes da empresa selecionada na lista
 */

public class SingleEnterpriseActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView mImage;
    private ImageView mActionBackButton;
    private TextView mActionEnterpriseName;

    private TextView mNoImageText;
    private TextView mEnterpriseDescription;
    private Enterprise mEnterprise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_enterprise);

        mToolbar = (Toolbar) findViewById(R.id.include);
        mImage = (ImageView) findViewById(R.id.image_single);
        mNoImageText = (TextView) findViewById(R.id.no_image_text);

        //pega os dados da empresa passados pela HomeActivity
        mEnterprise = (Enterprise) getIntent().getSerializableExtra("enterprise");

        mEnterpriseDescription = (TextView) findViewById(R.id.description_single);
        mEnterpriseDescription.setMovementMethod(new ScrollingMovementMethod()); //ativa o scroll caso a descrição seja grande

        setSupportActionBar(mToolbar);
        ActionBar action = getSupportActionBar();

        action.setDisplayShowCustomEnabled(true); //habilita que uma custom view seja adicionada ao toolbar
        action.setCustomView(R.layout.details_bar);//adiciona a custom view ao toolbar
        action.setDisplayShowTitleEnabled(false); //esconde o título

        mActionBackButton = (ImageView)action.getCustomView().findViewById(R.id.back);
        mActionEnterpriseName = (TextView)action.getCustomView().findViewById(R.id.enterprise_name_single);

        loadInfo();
        mActionBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleEnterpriseActivity.this.finish();
            }
        });
    }

    /**
     * Método loadInfo irá carregar os detalhes da empresa nas Views dessa activity
     * caso a empresa não tenha uma imagem cadastrada no servidor, uma caixa de texto personalizada
     * com o nome da empresa será mostrada, foi utilizada a bilbioteca Glide para carregar as imagens
     */

    public void loadInfo(){
        mEnterpriseDescription.setText(mEnterprise.getDescription());
        mActionEnterpriseName.setText(mEnterprise.getEnterpriseName());

        //se não tem imagem
        if(mEnterprise.getPhoto_url()==null){
            mImage.setVisibility(View.INVISIBLE);
            mNoImageText.setText(mEnterprise.getEnterpriseName());
        }
        else{
            mNoImageText.setVisibility(View.INVISIBLE);
            Glide.with(this)
                    .load(mEnterprise.getPhoto_url())
                    .override(318,155)
                    .into(mImage);
        }

    }


}
