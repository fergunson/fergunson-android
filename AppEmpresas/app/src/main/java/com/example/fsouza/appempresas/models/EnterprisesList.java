package com.example.fsouza.appempresas.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by fsouza on 15/07/17.
 * Classe EnterprisesList que irá montar uma lista de empresas, é necessário sua existencia no momento
 * de recuperação dos dados no GET dado ao servidor no momento de busca, cada instância dessa classe
 * irá armazenar os resultados de uma busca.
 */

public class EnterprisesList implements Serializable {

    @SerializedName("enterprises")
    private ArrayList<Enterprise> enterpriseList;

    public EnterprisesList() {
        enterpriseList = new ArrayList<Enterprise>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }

    @Override
    public String toString() {
        String empresas="";
        for (Enterprise e: enterpriseList) {
            empresas = empresas + e.getEnterpriseName()+"\n";
        }

        return empresas;
    }
}
