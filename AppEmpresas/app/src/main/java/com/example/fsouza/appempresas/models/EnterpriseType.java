package com.example.fsouza.appempresas.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fsouza on 15/07/17.
 * Classe EnterpriseType com o tipo de empresa, seu id e seu ramo de atuação
 */

public class EnterpriseType implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("enterprise_type_name")
    private String typeName;

    public EnterpriseType(int id, String typeName) {
        this.id = id;
        this.typeName = typeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
