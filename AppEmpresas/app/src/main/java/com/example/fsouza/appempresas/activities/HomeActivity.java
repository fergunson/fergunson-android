package com.example.fsouza.appempresas.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fsouza.appempresas.R;
import com.example.fsouza.appempresas.adapter.EnterpriseAdapter;
import com.example.fsouza.appempresas.controller.SearchController;
import com.example.fsouza.appempresas.models.AuthUser;
import com.example.fsouza.appempresas.models.Enterprise;
import com.example.fsouza.appempresas.models.EnterprisesList;

import java.util.ArrayList;

/**
 * Classe HomeActivity, é a tela principal do aplicativo, nela é possível realizar buscas por empresas
 * e conferir a sua lista de resultados, assim como escolher qual empresa você quer ver mais detalhes sobre
 * possui um Toolbar com um ícone de busca, onde o usuário irá entrar com o nome das empresas a ser pesquisadas
 */

public class HomeActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private MenuItem mSearchAction;
    private EditText mConteudoBusca;
    private TextView mHomeText;
    private ListView mListaEmpresas;

    private SearchController searchController;

    private boolean isSearchOpened;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mConteudoBusca = (EditText) findViewById(R.id.edtSearch);
        mHomeText = (TextView) findViewById(R.id.homeTextSearch);
        mListaEmpresas= (ListView) findViewById(R.id.list);

        //recebe os dados de autenticação da LoginActivity para autenticação na API da iOasys
        AuthUser authUser = (AuthUser) getIntent().getSerializableExtra("auth");
        searchController = new SearchController(authUser); //cria o controlador para busca e passa os dados de autenticação

        setSupportActionBar(mToolbar);
        onpenHomeToolbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        handleMenuSearch();
        return super.onOptionsItemSelected(item);
    }

    /**
     * Método que lida quando o usuário abre a barra de buscas
     */
    protected void handleMenuSearch(){
        ActionBar action = getSupportActionBar(); //pega a actionbar
        action.setDisplayShowTitleEnabled(false); //esconde o título

        if(isSearchOpened){ //confere se a busca já estava aberta

            onpenHomeToolbar(); //fecha a barra de buscas e mostra o logo da iOasys

            hideKeyboard(); //esconde o teclado
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_search_copy)); //muda o ícone de busca para a lupa

            isSearchOpened = false;
        } else { //abre a barra de buscas

            action.setDisplayShowCustomEnabled(true); //habilita para mostrar uma custom view na ActionBar
            action.setCustomView(R.layout.search_bar);//adiciona a custom view de busca

            mConteudoBusca = (EditText)action.getCustomView().findViewById(R.id.edtSearch); //o editText onde a busca é feita

            //listener para fazer a busca caso o usuário clique em buscar
            mConteudoBusca.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        searchController.fazBusca(v.getText().toString(),HomeActivity.this); //chama o método faz busca do controller
                        //com a string de busca como parâmetro

                        hideKeyboard(); //esconde o teclado
                        mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_search_copy));
                        return true;
                    }
                    return false;
                }
            });


            mConteudoBusca.requestFocus();

            //abre o teclado focado no edit text
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mConteudoBusca, InputMethodManager.SHOW_IMPLICIT);


            //muda o ícone para o símbolo de "fechar"
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_close));

            isSearchOpened = true;
        }
    }

    /**
     * Método busca feita que será chamado pelo controlador após a busca ser realizada, irá mostrar una
     * lista com as empresas resultantes da busca, ou dizer que nenhum resultado foi encontrado
     * @param enterprisesList EnterprisesList lista de empresas obtida
     */
    public void buscaFeita(EnterprisesList enterprisesList){
        onpenHomeToolbar(); //mostra o tolbar com o logo da iOasys e fecha a aba de busca
        mHomeText.setVisibility(View.INVISIBLE);

        final ArrayList<Enterprise> enterprises = enterprisesList.getEnterpriseList(); //pega a lista de empresas

        if(enterprises.size()==0){ //se a lista está vazia
            showNoResults(); //mostra que nenhum resultado foi obtido
        }
        else {
            //cria o adapter para mostrar a lista de empresas
            EnterpriseAdapter enterpriseAdapter = new EnterpriseAdapter(enterprises, getBaseContext());
            mListaEmpresas.setAdapter(enterpriseAdapter);

            mListaEmpresas.setVisibility(View.VISIBLE);

            //listener caso o usuário clique em algo da lista e queira ver detalhes de uma empresa específica
            mListaEmpresas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //chama a tela com detalhes da empresa e passa a empresa como parâmetro
                    Intent intent = new Intent(HomeActivity.this, SingleEnterpriseActivity.class);
                    intent.putExtra("enterprise", enterprises.get(position));

                    startActivity(intent);
                }
            });
        }
    }

    /**
     * Método openHomeToolbar muda o layout do toolbar para o logo da iOasys e o botão de abrir busca
     */
    public void onpenHomeToolbar(){

        ActionBar action = this.getSupportActionBar();

        action.setDisplayShowCustomEnabled(true); //disable a custom view inside the actionbar
        action.setCustomView(R.layout.home_bar);
        action.setDisplayShowTitleEnabled(false); //esconde o título
    }

    /**
     * Método que esconde o teclado
     */
    public void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mConteudoBusca.getWindowToken(), 0);
    }

    /**
     * Método showNoResults caso nenhum resultado seja obtido na busca, diz ao usuário que não foi possível obter
     * resultados e qual foi a sua busca
     */
    public void showNoResults(){
        mHomeText.setVisibility(View.VISIBLE);
        mHomeText.setText(getString(R.string.nenhum_resultado)+mConteudoBusca.getText().toString());
    }

    /**
     * Método erroMessage genérico que mostra o erro ocorrido em forma de toast
     * @param erro String com o conteúdo da mensagem de erro
     */
    public void errorMessage(String erro){
        Toast.makeText(getBaseContext(),getString(R.string.erro)+ erro,Toast.LENGTH_LONG).show();
    }
}
