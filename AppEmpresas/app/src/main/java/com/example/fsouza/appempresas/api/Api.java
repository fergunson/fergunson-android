package com.example.fsouza.appempresas.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fsouza on 15/07/17.
 * Classe api com os dados da Api da iOasys fornecidas no escopo do projeto
 * endereço do servidor e versão atual da api, essa classe irá criar um objeto Retrofit
 * que irá nos auxiliar a comunicar com este servidor
 */

public class Api {
    private static final String ENDPOINT = "http://54.94.179.135:8090";
    private static final String VERSION = "v1";

    public Retrofit getRetrofit(){
        // Trailing slash is needed

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT+"/api/"+VERSION+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
