package com.example.fsouza.appempresas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fsouza.appempresas.R;
import com.example.fsouza.appempresas.models.Enterprise;

import java.util.ArrayList;

/**
 * Created by fsouza on 24/05/17.
 * Adapter para a lista de empresas, com a imagem (caso exista), nome, país e área de atuação
 * caso não haja uma imagem cadastrada no server, mostra uma caixa de texto personalizada com o
 * nome da empresa, utilizou-se da biblioteca Glide para carregar as imagens
 */

public class EnterpriseAdapter extends BaseAdapter {
    private ArrayList<Enterprise> enterprises;
    private LayoutInflater layoutInflater;
    private Context context;

    public EnterpriseAdapter(ArrayList<Enterprise> enterprises, Context context) {
        this.context=context;
        this.layoutInflater = (LayoutInflater.from(context));
        this.enterprises = enterprises;
    }

    @Override
    public int getCount() {
        return enterprises.size();
    }

    @Override
    public Object getItem(int position) {
        return enterprises.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = layoutInflater.inflate(R.layout.fragment_empresa,null);

        Enterprise enterprise = enterprises.get(position);

        ImageView imageView;
        imageView = (ImageView) view.findViewById(R.id.enterprise_image);

        TextView noImageText;
        noImageText = (TextView) view.findViewById(R.id.no_enterprise_image);

        //se não tem imagem
        if (!(enterprise.getPhoto_url()==null)){
            imageView.setVisibility(View.INVISIBLE);
            noImageText.setText(enterprise.getEnterpriseName());
        }
        else {
            noImageText.setVisibility(View.INVISIBLE);
            Glide.with(context)
                    .load(enterprise.getPhoto_url())
                    .override(105,80)
                    .into(imageView);
        }

        TextView enterpriseName;
        enterpriseName = (TextView) view.findViewById(R.id.enterprise_name);
        enterpriseName.setText(enterprise.getEnterpriseName());

        TextView enterpriseType;
        enterpriseType = (TextView) view.findViewById(R.id.enterprise_type);
        enterpriseType.setText(enterprise.getEnterpriseType().getTypeName());

        TextView enterpriseCountry;
        enterpriseCountry = (TextView) view.findViewById(R.id.enterprise_country);
        enterpriseCountry.setText(enterprise.getCountry());

        return view;
    }
}