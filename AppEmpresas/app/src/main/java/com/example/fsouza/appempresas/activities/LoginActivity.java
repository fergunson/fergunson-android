package com.example.fsouza.appempresas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fsouza.appempresas.R;
import com.example.fsouza.appempresas.controller.LoginController;
import com.example.fsouza.appempresas.models.AuthUser;

/**
 * Classe LoginActivity é a primeira a ser executada no aplicativo,
 * ela possui campos para que o usuário insira seu login, senha e mostra na tela o resultado
 * desse login, caso tenha sido bem sucedido vai para a tela Home, caso não seja bem sucedido mostra
 * a razão para o usuário.
 */

public class LoginActivity extends AppCompatActivity {
    private EditText mUserEmail;
    private EditText mUserPassword;
    private Button mSubmitButton;
    private LoginController loginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUserEmail = (EditText) findViewById(R.id.editTextEmail);
        mUserPassword = (EditText) findViewById(R.id.editTextPassword);
        mSubmitButton = (Button) findViewById(R.id.submit_button);
        loginController = new LoginController(); //cria o controller de login

        /**
         * Listener do botão enviar, para fazer o login do usuário
         */
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //confere se email ou senha estão em branco
                if(loginController.loginCheck(mUserEmail.getText().toString(),
                        mUserPassword.getText().toString())){

                    loginController.login(mUserEmail.getText().toString(),
                            mUserPassword.getText().toString(),LoginActivity.this);

                }
                else {
                    errorMessage(getString(R.string.email_senha_vazio));
                }


            }
        });
    }

    /**
     * Método erroMessage genérico que mostra o erro ocorrido em forma de toast
     * @param erro String com o conteúdo da mensagem de erro
     */
    public void errorMessage(String erro){
        Toast.makeText(getBaseContext(),getString(R.string.erro)+ erro,Toast.LENGTH_LONG).show();
    }

    /**
     * Método callHome caso o LoginController realize um login bem sucedido, mostra a mensagem
     * de login bem sucedido ao usuário e passa para a tela Home o AuthUser com os tokens de autenticação
     * @param authUser
     */

    public void callHome(AuthUser authUser){
        Toast.makeText(this,getString(R.string.sucesso),Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.putExtra("auth", authUser);

        startActivity(intent);
    }
}
