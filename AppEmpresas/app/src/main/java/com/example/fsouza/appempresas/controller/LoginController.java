package com.example.fsouza.appempresas.controller;

import android.util.Log;

import com.example.fsouza.appempresas.R;
import com.example.fsouza.appempresas.activities.LoginActivity;
import com.example.fsouza.appempresas.api.Api;
import com.example.fsouza.appempresas.api.ApiService;
import com.example.fsouza.appempresas.models.AuthUser;
import com.example.fsouza.appempresas.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by fsouza on 17/07/17.
 * Classe LoginController responsável pelo controle de toda as regras de negócio
 * responsáveis pelo recebimento de um usuário e senha da LoginActivity, fazer a comunicação com a
 * API da iOasys e receber os 3 Tokens para autenticação. Utiliza da API Retrofit para fazer um
 * POST com o login e senha.
 */

public class LoginController {
    private Retrofit retrofit;
    private User user;
    private final ApiService apiService;

    /**
     * Construtor da LoginController
     */

    public LoginController() {
        Api api = new Api();
        retrofit = api.getRetrofit(); // objeto da API retrofit que irá nos auxiliar na comunicação com o servidor
        apiService = retrofit.create(ApiService.class); //interface de comunicação com a API
    }

    /**
     * Método login, recebe o email e senha disponibilizados pelo usuário, além da instância da LoginActivity
     * que passa esses parâmetros, de onde chamaremos métodos de acordo com os resultados obtidos no login
     * @param email String email com o email do usuário
     * @param password String senha com a senha do usuário
     * @param loginActivity LoginActivity instância que o usuário fez o login, para que possemos responder as informações
     */

    public void login(String email,
                          String password,
                          final LoginActivity loginActivity) {

        if (loginCheck(email,password)){ //checa se o login ou senha estão em branco

            user = new User(email, password); //cria um novo usuário com os dados de Login e Senha

            //a partir daqui utilizamos a API retrofit para fazer um POST no servidor da API iOasys
            //colocamos o login e senha no formato jSon no body, a partir da resposta, criamos um
            //usuário AuthUser com os 3 tokens para autenticação

            Call call = apiService.validateUser(user);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    //se o login foi realizado com sucesso criamos um objeto AuthUser e transferimos para a HomeActivity
                    if (response.isSuccessful()) {
                        AuthUser authUser = new AuthUser(response.headers().get("access-token"),
                                response.headers().get("client"),
                                response.headers().get("uid"));

                        Log.d("acess-token",response.headers().get("access-token"));
                        Log.d("client",response.headers().get("client"));
                        Log.d("uid",response.headers().get("uid"));
                        Log.d("Sucesso: ", "Sucesso!!");
                        loginActivity.callHome(authUser);
                    }

                    //se o login foi mal sucedido mostramos a mensagem para o usuário
                    else {
                        loginActivity.errorMessage(loginActivity.getString(R.string.email_senha_incorretos));
                    }
                }

                // se ocorrer algum erro no acesso ao servidor mostramos esse erro ao usuário
                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.e("ERRO ", t.getMessage());
                    loginActivity.errorMessage(t.getMessage());
                }
            });
        }
    }

    /**
     * Método login check, chega se o email e senha inseridos estão em branco
     * @param email String email inserido pelo usuário
     * @param senha String senha inserida pelo usuário
     * @return Boolean false se algum campo estiver em branco, verdadeiro se os campos estão preenchidos
     */
    public boolean loginCheck(String email, String senha){
        if(email.isEmpty() || senha.isEmpty()){
            return false;
        }
        else {
            return true;
        }
    }
}
