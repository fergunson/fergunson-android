package com.example.fsouza.appempresas.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fsouza on 14/07/17.
 * Classe Enterprise descreve uma empresa da lista, cada objeto desta classe representa um Json
 * semelhante ao abaixo
 *
 {
     "id": 4,
     "email_enterprise": null,
     "facebook": null,
     "twitter": null,
     "linkedin": null,
     "phone": null,
     "own_enterprise": false,
     "enterprise_name": "AQM S.A.",
     "photo": null,
     "description": "Cold Kilshare_priceshare_priceler was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ",
     "city": "Maule",
     "country": "Chile",
     "value": 0,
     "share_price": 5000,
     "enterprise_type": {
        "id": 1,
        "enterprise_type_name": "Agro"
        }
 }
 */

public class Enterprise implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("email_enterprise")
    private String email;

    @SerializedName("facebook")
    private String facebook;

    @SerializedName("twitter")
    private String twitter;

    @SerializedName("linkedin")
    private String linkedin;

    @SerializedName("phone")
    private String phone;

    @SerializedName("own_enterprise")
    private Boolean ownEnterprise;

    @SerializedName("enterprise_name")
    private String enterpriseName;

    @SerializedName("photo")
    private String photo_url;

    @SerializedName("description")
    private String description;

    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;

    @SerializedName("value")
    private float value;

    @SerializedName("share_price")
    private float sharePrice;

    @SerializedName("enterprise_type")
    private EnterpriseType enterpriseType;

    public Enterprise(int id, String email, String facebook, String twitter, String linkedin,
                      String phone, Boolean ownEnterprise,String enterpriseName,String photo_url, String description, String city,
                      String country, float value, float sharePrice, EnterpriseType enterpriseType) {
        this.id = id;
        this.email = email;
        this.facebook = facebook;
        this.twitter = twitter;
        this.linkedin = linkedin;
        this.phone = phone;
        this.ownEnterprise = ownEnterprise;
        this.enterpriseName= enterpriseName;
        this.photo_url= photo_url;
        this.description = description;
        this.city = city;
        this.country = country;
        this.value = value;
        this.sharePrice = sharePrice;
        this.enterpriseType = enterpriseType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getOwnEnterprise() {
        return ownEnterprise;
    }

    public void setOwnEnterprise(Boolean ownEnterprise) {
        this.ownEnterprise = ownEnterprise;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(float sharePrice) {
        this.sharePrice = sharePrice;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }
}
