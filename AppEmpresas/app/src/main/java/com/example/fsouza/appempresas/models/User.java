package com.example.fsouza.appempresas.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fsouza on 15/07/17.
 * Classe User onde cada instância armazena os dados de login e senha de um usuário
 */

public class User {
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
