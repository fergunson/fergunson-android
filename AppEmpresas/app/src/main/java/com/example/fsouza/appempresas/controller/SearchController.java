package com.example.fsouza.appempresas.controller;

import android.util.Log;

import com.example.fsouza.appempresas.activities.HomeActivity;
import com.example.fsouza.appempresas.api.Api;
import com.example.fsouza.appempresas.api.ApiService;
import com.example.fsouza.appempresas.models.AuthUser;
import com.example.fsouza.appempresas.models.EnterprisesList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by fsouza on 18/07/17.
 * Classe SearchController, responsável por comunicar com a API, fazer o GET no servidor com os tokens
 * de acesso do AuthUser como cabeçalho e o Query com o conteúdo da busca, os resultados são obtidos em forma
 * de uma lista de empresas
 */

public class SearchController {

    private Retrofit retrofit;
    private final ApiService apiService;
    private AuthUser authUser;

    /**
     * Construtor do SearchController, recebe o usuário Auth que detém os 3 tokens para autenticação
     * no servidor, também ira inicializar objetos da classe Api, Api service e retrofit, que irão
     * auxiiar na comunicação.
     * @param authUser AuthUser instância que detém os dados para autenticação no server
     */

    public SearchController(AuthUser authUser){
        Api api = new Api();
        retrofit = api.getRetrofit();
        apiService = retrofit.create(ApiService.class);
        this.authUser=authUser;
    }

    /**
     * Método fazBusca responsável pela autenticação dos tokens no servidor, e pelo GET onde iremos
     * obter uma lista de empresas no resultado da busca
     * @param searchEntry String com o valor da busca
     * @param homeActivity HomeAcitivity com a instância da activity onde iremos mostrar os resultados da busca
     */

    public void fazBusca(String searchEntry, final HomeActivity homeActivity){

        if (authUser!=null) {
            Call callTwo = apiService.enterpriseList(authUser.getAccessToken(),
                    authUser.getClient(),
                    authUser.getUid(), searchEntry);

            callTwo.enqueue(new Callback<EnterprisesList>() {
                @Override
                public void onResponse(Call<EnterprisesList> call, Response<EnterprisesList> response) {
                    Log.d("Body ", response.body().toString());
                    homeActivity.buscaFeita(response.body());
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    homeActivity.errorMessage(t.getMessage());
                }
            });
        }
    }
}
